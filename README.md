# Wireguard VPN for home use ( Einrichten eines Wireguard VPN Tunnels ins heimische Netzwerk. )
[comment]: <> (To add screenshots)
[comment]: <> (<img alt="Wireguard logo" src="https://gitlab.com/onkelhartwig/wireguard-vpn-for-home-use/-/raw/main/wireguard.svg?inline=false">)

Das Script ist für Heimanwender ohne tieferes Wissen rund um Netzwerke gedacht. Es soll auf einfache Weise die Möglichkeiten bieten mit Hilfe von Wirdguard den Zugriff auf das heimische Netzwerk zu ermöglichen.


## Einleitung

In der Vergangenheit benutzte ich OpenVPN um VPNs einzurichten. Meine ersten Erfahrungen habe ich mit OpenVPN durch PiVPN gemacht und die generierten Konfiguationen nach und nach an meine, über die Möglichkeiten von PiVPN hinausgehenden, Anforderungen angepasst.
Als ich vor kurzem festgestellt habe, dass die neue Version von PiVPN nun standardmäßig Wireguard anstelle von OpenVPN benutzt,
dachte ich mit es sei an der Zeit sich auch mit Wireguard näher zu befassen. Mein Hauptinteresse lag in der versprochenen Leistungssteigerung von Wireguard gegenüber OpenVPN.
Inzwischen habe ich folgende Erkenntnisse gewonnen:
- Wireguard ist tatsächlich um Längen performanter als OpenVPN. Bei einer 100Mbit/s Verbindung erreicht OpenVPN einen Durchsatz von ca. 35Mbit/s während Wireguard es auf stattlich 90Mbit/s bringt.
- Der Konfigurationsaufwand für OpenVPN ist beträchtlich, die Dokumentation allerdings auch ausgereift. Recherchen im Interet, sorry Google, sind sehr oft veraltet. Wireguard hingegen ist eher mäßig dokumentiert, allerdings für Standardaufgaben einfach zu konfigurieren.
- OpenVPN kann mit TCP oder UDP betrieben werden, während Wireguard nur UDP unterstützt. UDP stellt generell eine schnellere Verbindung zur Verfügung aber das "Verstecken" eines VPN hinter Port 443 (https) ist unter Wireguard nicht möglich. Bei OpenVPN ist der Netzwerkverkehr des VPN nivht ohne weiteres von normalen https Traffic unterscheidbar.
- Hier eine Warnung: Fehlkonfigurationen können ein Gerät, Windows PC, Linux Server oder Handy, komplett aus dem Netzwerk ausschließen. Eine Sicherung der funktionierenden Konfiguration ist, wie immer, dringend empfohlen.

Ein Wort am Rande: Geschwindigkeitsrekorde sind bei keinem VPN der Welt zu erwarten. 

Das absolute Limit wird durch die Upload Geschwindigkeit ihres heimischen Routers und die auf dem Gerät verfügbare Geschwindigkeit limitiert (der schlechtere Wert ist das Limit). Heute gängige DSL oder Kabel Tarife bieten in der Regel zwischen 5 und 25 Mbit/s als Upload (das wird erst bei näheren Lesen der Bedingungen sichtbar, geworben wird mit hohen Download Geschwindigkeiten) zur Verfügung. 

Videos in HD zu schauen kostet ca. 10Mbit/s währen UHD sogar 16Mbit/s erfordert. Da sich alle Clients im VPN die Bandbreite ihres heimischen Anschlusses teilen müssen, ist eine zu große Anzahl von Clients nicht hilfreich.

## Einbindung ins Heimnetz

Aus den genannten Gründen habe ich mich entschlossen, von OpenVPN zu Wireguard zu wechseln. Mein Heimnetzwerk sieht folgendermaßen aus.
- Kabel Anschluss mit 250Mbit/s Download und 50 Mbit/s Upload. Hardware ist eine Fritzbox 6660 Cable.
- Per Ethernet Kabel angeschlossen ein DLAN Pärchen um über die Stromleitung in entferntere (3 Wände) Bereiche der Wohnung zu gelangen. Der im Arbeitszimmer angeschlossene DLAN Adapter dient als Repeater für die Fritzbox.
- Per Kabel an der Fritzbox sind angeschlossen:
  - ein Raspberry Pi, der für den VPN zuständig ist und automatisiert Aufgaben erledigt.
  - eine NAS Box (My Cloud Ex2 Ultra)
- Diverse Mobilgeräte (Handys und Tablets)
- ein Desktop PC und 3 Laptops

Darüber hinaus verfüge ich über einen Server im Internet um dort 
- via Nextcloud meine Daten zu syncronisieren
- via Squid einen Proxy bereitzustellen
- über PiHole die Werbung zu filtern, wenn man den Proxy benutzt

## Add your files

Hier plane ich eine Anleitung für die Einrichtung der erforderlich Port Freigabe

## Download and install

Übersetzung fehlt !!

Die Software wurde unter "Raspbian GNU/Linux 11 (bullseye)" auf einem Raspberry PI entwickelt und getestet. Die Installation erfolgt unter jedem Debian basierten Linux (z.B. Ubuntu) gleich.
Die Installation des Raspberry PI selbst beschreibe ich hier nicht, es gibt schon mehr als genug solcher Anleitungen.
Die benötigte Software wird wie folgt installiert:
  ```bash
  sudo apt-get install wireguard qrencode
  ```
Damit ist sämtliche benötigte Software installiert.
Laden sie nun das Projekt als .tar.gz file herunter
  ```bash
  cd $HOME
  mkdir wireguard && cd wireguard
  tar --strip-components 2 -xzf /path_to_download_folder/wireguard-vpn-for-home-use-main-wireguard.tar.gz wireguard-vpn-for-home-use-main-wireguard/wireguard/
  ```
Sie haben nun ein neues Directory Namens "wireguard" in Ihrem Home Directory. Dieses Directory enthält alles erforderliche um das Konfigurationsprogramm zu starten. Bitte ändern Sie nicht an den Unterverzeichnissen, neue dürfen aber angelegt werden.

Passen Sie das Configurations File etc/config.sh an Ihre Bedürfnisse an (oder legen Sie ein Neues an) und starten sie die Software
  ```bash
  bin/create_config.sh config.sh # config.sh ist der Dateiname ihrer Konfigurationsdatei
  ```

## Installation

In your wireguard folder you will find a directory structure
wireguard/ - working directory
  bin/ - executables 
  etc/ - configuration files
  var/ - log files
  tmp/ - temporary files
A sample configuration file can be found in etc/. The file includes comments for each parameter.

## Usage
- Adapt or create a configuration file to your needs
- run bin/create_config.shell
- you will find a new directory in the working directory containing everything needed to start your Wireguard VPN
- copy all files from this directory to /etc/wireguard as root user
- start your VPN by issuing
  ```bash
  systemctl start wg-quick up <network name>
  ```
For the clients you need to follow the instructions per operating system. The software must be installed first using the [Wireguard download site](https://www.wireguard.com/install/)

In general you need to import the configuration file into the Wireguard app on your client device. 
To make this easier on mobile devices a QR-code can be generated by
```bash
qrencode -t ansiutf8 -M  < networkname/cli200_networkname.conf
```
## Support
Wenn mit dem Script Probleme auftreten, schildern Sie diese bitte und senden eine Mail an [mich](mailto:hartwig.onkelhartwig.de)

## Roadmap
Die erste Version ist vollständig und lauffähig. Allerdings fehlt noch jede Menge Dokumentation (Installationsanweisung, Usage Guide etc. :-(

## Authors and acknowledgment
Die Software wurde von Hartwig Becker erstellt und ist, wie in der Lizenz sichtbar, als "gekauft wie gesehen" zu betrachten. Ich übernehme keinerlei Haftung die durch die Nutzung der Software möglicherweise entstehen.

## Project status
- Coding ist erledigt
- Installationsanweisung ist in Arbeit
- Hinweise zu Benutzung sind in Arbeit
