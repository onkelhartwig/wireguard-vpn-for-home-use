# Wireguard VPN for home use
[comment]: <> (To add screenshots)
[comment]: <> (<img alt="Wireguard logo" src="https://gitlab.com/onkelhartwig/wireguard-vpn-for-home-use/-/raw/main/wireguard.svg?inline=false">)


Create Wireguard VPN server and client configuration for home use. 
Goal of the game is to address users with no or littke knowledge about networking.

## Getting started

In the past I used OpenVPN for tunneling. The first experiences were made with PiVPN and after a while I updated the OpenVPN configuration without PiVPN usage. The examples, guides and documentation is pretty fine.
Recognizing that PiVPN now uses Wireguard as preferred backend I did some tests outside PiVPN with Wireguard. My findings are
- Wireguard performs significat faster as OpenVPN. My measurements show 35 Mbit/s for OpenVPN while Wireguard reaches 90 Mbit/s on a 100 Mbit intert connection
- OpenVPN requires a lot more of configuration effort and, sorry google, a lot of examples and guides are out-of-date.
- OpenVPN can act on the TCP and UDP protocol while Wireguard is limited to UDP. In general UDP performs better but TCP in OpenVPN can be used to "hide" traffic when using port 443. The OpenVPN traffic is quiet similar to normal https traffic of your browser
- Both, OpenVPN and Wireguard can lock you out of your server if you make mistakes in the configuraition. Especially clients are affected from this 
<<<<<<< Updated upstream
=======

For the above reasons I switched my tunnels from OpenVPN to Wireguard with success. I have the following configuration which is used for testing:
- My home internet connection comes on a Fritz!Box 6660 cable with dynamic IPv4 address
- I have a server in the internet (https://www.onkelhartwig,de) running Pi-hole, squid and Apache web server
- A lot of clients need acces to the home network
  - My private Iphone and Ipad
  - My wifes devices
  - My family devices
  - My internet server

There are different purposes to use the VPN
- e.g. to view German Fußball from foreign countries, to enable home banking from outside (my bank is blocks IPs from several countries like Nigeria)
- Access the NAS device with our files, music, videos etc. when not at home

To enable both purposes we need 2 different types of clients. 
- TUNNEL_ONLY which means that only access to my home network is given
- COMPLETE_TRAFFIC to route all network traffic through the VPN

Afte bringing this all to work, I recognized some effort to expand the VPN network step by step. The most comfortable approach is to generate all neccessary config files in on step to enable the user to easily distribute the access to others. 
A simple shell script should be used by interpreting a small initialization file.


## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:0dc888f90957354875d8d28e046aecf4?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:0dc888f90957354875d8d28e046aecf4?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:0dc888f90957354875d8d28e046aecf4?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/onkelhartwig/wireguard-vpn-for-home-use.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:0dc888f90957354875d8d28e046aecf4?https://gitlab.com/onkelhartwig/wireguard-vpn-for-home-use/-/settings/integrations)
>>>>>>> Stashed changes

For the above reasons I switched my tunnels from OpenVPN to Wireguard with success. I have the following configuration which is used for testing:
- My home internet connection comes on a Fritz!Box 6660 cable with dynamic IPv4 address
- I have a server in the internet [onkelhartwig.de](https://www.onkelhartwig,de) running Pi-hole, squid and Apache web server
- A lot of clients need acces to the home network
  - My private Iphone and Ipad
  - My wifes devices
  - My family devices
  - My internet server

There are different purposes to use the VPN
- e.g. to view German Fußball from foreign countries, to enable home banking from outside (my bank is blocks IPs from several countries like Nigeria)
- Access the NAS device with our files, music, videos etc. when not at home

To enable both purposes we need 2 different types of clients. 
- TUNNEL_ONLY which means that only access to my home network is given
- COMPLETE_TRAFFIC to route all network traffic through the VPN

After bringing this all to work, I recognized some effort to expand the VPN network step by step. The most comfortable approach is to generate all neccessary config files in on step to enable the user to easily distribute the access to others. 
A simple shell script should be used by interpreting a small initialization file.

All tests were done on a Raspberry Pi 4 with Raspbian Bullseye (11), on Debian Bullseye and on several IOS devices (Iphone/Ipad). 

## Download and install

The installations can be done on any Linux system (or cygwin - I need to test this) with an acceptable version.
Simply download the folder "wireguard" as zip file and unpack on a location of your choice, e.g. your home directory.

## Installation

In your wireguard folder you will find a directory structure
wireguard/ - working directory
  bin/ - executables 
  etc/ - configuration files
  var/ - log files
  tmp/ - temporary files
A sample configuration file can be found in etc/. The file includes comments for each parameter.

## Usage
- Adapt or create a configuration file to your needs
- run bin/create_config.shell
- you will find a new directory in the working directory containing everything needed to start your Wireguard VPN
- copy all files from this directory to /etc/wireguard as root user
- start your VPN by issuing
  ```bash
  systemctl start wg-quick up <network name>
  ```
For the clients you need to follow the instructions per operating system. The software must be installed first using the [Wireguard download site](https://www.wireguard.com/install/)

In general you need to import the configuration file into the Wireguard app on your client device. 
To make this easier on mobile devices a QR-code can be generated by
```bash
qrencode -t ansiutf8 -M  < networkname/cli200_networkname.conf
```



## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

