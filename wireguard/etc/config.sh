# Config file for Wireguard network configuration
# The file is sourced by the creation script, so empty lines and lines starting with "#"-sign
# are ignored
# The values of the variables should be constants
#
# Be aware that all participating peers in the VPN (including the server) will share your
# UPLOAD speed. E.g. a data plan with 64Mbit/s download and 10Mbit/s upload speed will limit the
# VPN speed to 10Mbit/s !!

# Name of the Network, an adapter with this name will be used. In most of all examples you will
# find "wg0" as namei. The name must
#   not exceed 15 characters in length
#   consist only on lowercase letter, numbers and underscore (_)
# This parameter is required and must contain only [a-z], [0-9] and [_]
NETNAME="zuhause"

# Hostname / IP address of the server
# This parameter is required and must be a non-private IP or a valid DNS name (e.g. from DynDNS)
SERVERIP="exaple: zuhause.dyndns.org"

# The network to be created. The subnet must not exist on any client
# This parameter is required and must a valid IP address in CIDR notation
NETADDRESS="10.8.10.0/24"

# Port to be used for tunneling
# This parameter is required and point to a port forwarded on your router
NETPORT="51196"

# Number of clients only communication with the tunneled devices, normal internet access remains unchanged
# This parameter is required, the maximum of these clients is 50
NUMCLIENTS_TUNNEL_ONLY="19"

# Number of clients routing all their traffic through the tunnel
# This parameter is required, the maximum of these clients is 50
NUMCLIENTS_COMPLETE_TRAFFIC="20"

# comma separated IP range of your home network(s)
# HOMENET="" will only allow to connect within the VPN
HOMENET="192.168.178.0/24"

# which DNS should be used for clients routing complete traffic through the tunnel
# This parameter is required, internet connetion is not possible without DNS
DNS="9.9.9.9"

