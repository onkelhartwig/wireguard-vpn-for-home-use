#!/bin/bash
# update this line to your location
BASEDIR=/home/pi/wireguard

# Set directories
BINDIR=$BASEDIR/bin
CONFDIR=$BASEDIR/etc
TMPDIR=$BASEDIR/tmp
VARDIR=$BASEDIR/var

# Read the config file
. $CONFDIR/config.sh

# create the server config first
WG_DIR=$BASEDIR/$NETNAME
mkdir $WG_DIR || ( echo Interface already exist. Abort. && exit 1 )
echo Directory $WG_DIR created

# create key pairs for the server
umask 077
wg genkey >$WG_DIR/$NETNAME.priv
wg pubkey <$WG_DIR/$NETNAME.priv >$WG_DIR/$NETNAME.pub

# create key pairs for the clients
# first create the TUNNEL_ONLY key pairs
start=100
end=$(($start+$NUMCLIENTS_TUNNEL_ONLY-1))
for ((i=$start;i<=$end;i++))
do
   wg genkey >$WG_DIR/cli${i}_$NETNAME.priv
   wg pubkey <$WG_DIR/cli${i}_$NETNAME.priv >$WG_DIR/cli${i}_$NETNAME.pub
done

# now create the key pairs for COMPLETE_TRAFFIC clients
start=200
end=$(($start+$NUMCLIENTS_COMPLETE_TRAFFIC-1))
for ((i=$start;i<=$end;i++))
do
   wg genkey >$WG_DIR/cli${i}_$NETNAME.priv
   wg pubkey <$WG_DIR/cli${i}_$NETNAME.priv >$WG_DIR/cli${i}_$NETNAME.pub
done

# Create server.conf
cat <<EOF >>$WG_DIR/$NETNAME.conf
[Interface]
# Server $NETNAME
Address = $(echo $NETADDRESS | sed '1,$s?.0/?.1/?')
SaveConfig = false
ListenPort = $NETPORT
PrivateKey = $(cat $WG_DIR/$NETNAME.priv)

EOF

# create client.conf files for TUNNEL_ONLY clients and update the server.conf
start=100
end=$(($start+$NUMCLIENTS_TUNNEL_ONLY-1))
for ((i=$start;i<=$end;i++))
do
   # create client config
   cat <<EOF >$WG_DIR/cli${i}_$NETNAME.conf
[Interface]
# Client cli${i}_$NETNAME
PrivateKey = $(cat $WG_DIR/cli${i}_$NETNAME.priv)
Address = $(echo $NETADDRESS | cut -d . -f 1-3).$i/$(echo $NETADDRESS | cut -d / -f 2)
DNS = 9.9.9.9

[Peer]
# Server $NETNAME
PublicKey = $(cat $WG_DIR/$NETNAME.pub)
AllowedIPs = $NETADDRESS
Endpoint = $SERVERIP:$NETPORT
PersistentKeepalive = 30
EOF
   # End create client config

   # update server config
   cat <<EOF >>$WG_DIR/$NETNAME.conf
[Peer]
# Client cli${i}_$NETNAME
PublicKey = $(cat $WG_DIR/cli${i}_$NETNAME.pub)
AllowedIPs = $NETADDRESS

EOF
   # End update server config

done

# create the client.conf files for COMPLETE_TRAFFIC clients and update the server.conf
start=200
end=$(($start+$NUMCLIENTS_COMPLETE_TRAFFIC-1))
for ((i=$start;i<=$end;i++))
do
   :
   # create client config
   cat <<EOF >$WG_DIR/cli${i}_$NETNAME.conf
[Interface]
# Client cli${i}_$NETNAME
PrivateKey = $(cat $WG_DIR/cli${i}_$NETNAME.priv)
Address = $(echo $NETADDRESS | cut -d . -f 1-3).$i/$(echo $NETADDRESS | cut -d / -f 2)
DNS = 9.9.9.9

[Peer]
# Server $NETNAME
PublicKey = $(cat $WG_DIR/$NETNAME.pub)
AllowedIPs = $NETADDRESS,0.0.0.0/0
Endpoint = $SERVERIP:$NETPORT
PersistentKeepalive = 30
EOF
   # End create client config

   # update server config
   cat <<EOF >>$WG_DIR/$NETNAME.conf
[Peer]
# Client cli${i}_$NETNAME
PublicKey = $(cat $WG_DIR/cli${i}_$NETNAME.pub)
AllowedIPs = $NETADDRESS

EOF
   # End update server config

done


