#!/bin/bash

# evaluate ip adresses of a network in CIDR notation
net_to_ip()
{
   BASE_IP=${1%/*}
   IP_CIDR=${1#*/}

   if [ ${IP_CIDR} -lt 8 ]; then
      echo "Max range is /8."
      exit
   fi

   IP_MASK=$((0xFFFFFFFF << (32 - ${IP_CIDR})))

   IFS=. read a b c d <<<${BASE_IP}

   ip=$((($b << 16) + ($c << 8) + $d))

   ipstart=$((${ip} & ${IP_MASK}))
   ipend=$(((${ipstart} | ~${IP_MASK}) & 0x7FFFFFFF))

   seq ${ipstart} ${ipend} | while read i
   do
      echo $a.$((($i & 0xFF0000) >> 16)).$((($i & 0xFF00) >> 8)).$(($i & 0x00FF))
   done
}

create_client_keys() {
   # create key pairs for the clients
   # first create the TUNNEL_ONLY key pairs
   start=2
   startingip=$(net_to_ip $NETADDRESS | head -3 | tail -1)
   end=$(($start+$NUMCLIENTS_TUNNEL_ONLY+$NUMCLIENTS_COMPLETE_TRAFFIC-1))
   for ((i=$start;i<=$end;i++))
   do
      wg genkey >$WG_DIR/cli${i}_$NETNAME.priv
      wg pubkey <$WG_DIR/cli${i}_$NETNAME.priv >$WG_DIR/cli${i}_$NETNAME.pub
   done
}

create_server_config() {
   # Create server.conf

   # The server always needs to be the first address in a network
   # examples
   #   network = 10.8.0.0/24 first IP is 10.8.0.1
   #   network = 10.7.0.0/24 first IP is 10.7.0.1
   #   network = 10.7.0.0/23 first IP is 10.6.0.1
   #   network = 10.8.0.128/25 first IP is 10.8.0.129
   #   network = 10.8.0.133/25 first IP is 10.8.0.129
   startingip=$(net_to_ip $NETADDRESS | head -2 | tail -1)
   network=$(net_to_ip $NETADDRESS | head -1 )
   servernetmask=$(echo $NETADDRESS | cut -d "/" -f2)
   serverip="$startingip/$servernetmask"

   cat <<EOF >>$WG_DIR/$NETNAME.conf
[Interface]
# Server $NETNAME
Address = $startingip/$servernetmask
SaveConfig = false
ListenPort = $NETPORT
PrivateKey = $(cat $WG_DIR/$NETNAME.priv)

EOF

}

create_client_config() {
   startingip=$(net_to_ip $NETADDRESS | head -2 | tail -1)
   network=$(net_to_ip $NETADDRESS | head -1 )
   servernetmask=$(echo $NETADDRESS | cut -d "/" -f2)
   serverip="$startingip/$servernetmask"

   start=2
   end=$(($start+$NUMCLIENTS_TUNNEL_ONLY+$NUMCLIENTS_COMPLETE_TRAFFIC-1))
   for ((i=$start;i<=$end;i++))
   do
      if [ "$HOMENET" = "" ]
      then
         homemet=""
      else
         homenet=",$HOMENET"
      fi
      clientip=$(net_to_ip $network/$servernetmask | head -$(($i+1)) | tail -1)
      if [ "$i" -lt $(($start+$NUMCLIENTS_TUNNEL_ONLY)) ]
      then
         dns="# DNS = $DNS"
         allow="AllowedIPs = $network/$servernetmask$homenet"
      else
         dns="DNS = $DNS"
         allow="AllowedIPs = $network/$servernetmask$homenet",0.0.0.0/0
      fi
      cat <<EOF >$WG_DIR/cli${i}_$NETNAME.conf
[Interface]
# Client cli${i}_$NETNAME
PrivateKey = $(cat $WG_DIR/cli${i}_$NETNAME.priv)
Address = $clientip/$servernetmask
$dns

[Peer]
# Server $NETNAME
PublicKey = $(cat $WG_DIR/$NETNAME.pub)
$allow
Endpoint = $SERVERIP:$NETPORT
PersistentKeepalive = 30

EOF
      # Client config done, update server config
      cat <<EOF >>$WG_DIR/$NETNAME.conf
[Peer]
# Client cli${i}_$NETNAME
PublicKey = $(cat $WG_DIR/cli${i}_$NETNAME.pub)
AllowedIPs = $clientip/32

EOF
      #Server config updated

   done

}

# Set directories
BASEDIR="$(dirname $(dirname $0))"
BINDIR=$BASEDIR/bin
CONFDIR=$BASEDIR/etc
TMPDIR=$BASEDIR/tmp
VARDIR=$BASEDIR/var

# Read the config file
if [ "$1" = "" ]
then
   . $CONFDIR/config.sh
else
   . $CONFDIR/$1
   if [ "$?" != 0 ]
   then
      echo "error in configfile $CONFDIR/$1"
      exit 1
   fi
fi

# create the server config first
WG_DIR=$BASEDIR/$NETNAME
mkdir $WG_DIR
if [ "$?" -ne "0" ]
then
   exit 1
fi
echo Directory $WG_DIR created

# create key pairs for the server
umask 077
wg genkey >$WG_DIR/$NETNAME.priv
wg pubkey <$WG_DIR/$NETNAME.priv >$WG_DIR/$NETNAME.pub

# create key pairs for the clients
# first create the TUNNEL_ONLY key pairs
create_client_keys
### start=100
### end=$(($start+$NUMCLIENTS_TUNNEL_ONLY-1))
### for ((i=$start;i<=$end;i++))
### do
   ### wg genkey >$WG_DIR/cli${i}_$NETNAME.priv
   ### wg pubkey <$WG_DIR/cli${i}_$NETNAME.priv >$WG_DIR/cli${i}_$NETNAME.pub
### done

### # now create the key pairs for COMPLETE_TRAFFIC clients
### start=200
### end=$(($start+$NUMCLIENTS_COMPLETE_TRAFFIC-1))
### for ((i=$start;i<=$end;i++))
### do
   ### wg genkey >$WG_DIR/cli${i}_$NETNAME.priv
   ### wg pubkey <$WG_DIR/cli${i}_$NETNAME.priv >$WG_DIR/cli${i}_$NETNAME.pub
### done

# Create server.conf

# The server always needs to be the first address in a network
# examples
#   network = 10.8.0.0/24 first IP is 10.8.0.1
#   network = 10.7.0.0/24 first IP is 10.7.0.1
#   network = 10.7.0.0/23 first IP is 10.6.0.1
#   network = 10.8.0.128/25 first IP is 10.8.0.129
#   network = 10.8.0.133/25 first IP is 10.8.0.129
startingip=$(net_to_ip $NETADDRESS | head -2 | tail -1)
network=$(net_to_ip $NETADDRESS | head -1 )
servernetmask=$(echo $NETADDRESS | cut -d "/" -f2)
serverip="$startingip/$servernetmask"
echo Network is $network, ServerIP is $serverip
create_server_config

### cat <<EOF >>$WG_DIR/$NETNAME.conf
### [Interface]
### # Server $NETNAME
### Address = $startingip/servernetmask
### SaveConfig = false
### ListenPort = $NETPORT
### PrivateKey = $(cat $WG_DIR/$NETNAME.priv)

### EOF

# create client.conf files for TUNNEL_ONLY clients and update the server.conf
create_client_config
### start=2
### end=$(($start+$NUMCLIENTS_TUNNEL_ONLY-1))
### for ((i=$start;i<=$end;i++))
### do
###    cliip=$(net_to_ip $network | head $(($i+1)) | tail -1)
###    # create client config
###    cat <<EOF >$WG_DIR/cli${i}_$NETNAME.conf
### [Interface]
### # Client cli${i}_$NETNAME
### PrivateKey = $(cat $WG_DIR/cli${i}_$NETNAME.priv)
### # Address = $(echo $NETADDRESS | cut -d . -f 1-3).$i/32
### AAAA
### # DNS = 9.9.9.9
### 
### [Peer]
### # Server $NETNAME
### PublicKey = $(cat $WG_DIR/$NETNAME.pub)
### AllowedIPs = $NETADDRESS,$HOMENET
### Endpoint = $SERVERIP:$NETPORT
### PersistentKeepalive = 30
### EOF
###    # End create client config
### 
###    # update server config
###    cat <<EOF >>$WG_DIR/$NETNAME.conf
### [Peer]
### # Client cli${i}_$NETNAME
### PublicKey = $(cat $WG_DIR/cli${i}_$NETNAME.pub)
### AllowedIPs = $(echo $NETADDRESS | cut -d . -f 1-3).$i/32
### 
### EOF
###    # End update server config
### 
### done
### 
### # create the client.conf files for COMPLETE_TRAFFIC clients and update the server.conf
### start=$((2+$NUMCLIENTS_TUNNEL_ONLY-1))
### end=$(($start+$NUMCLIENTS_COMPLETE_TRAFFIC-1))
### for ((i=$start;i<=$end;i++))
### do
###    :
###    # create client config
###    cat <<EOF >$WG_DIR/cli${i}_$NETNAME.conf
### [Interface]
### # Client cli${i}_$NETNAME
### PrivateKey = $(cat $WG_DIR/cli${i}_$NETNAME.priv)
### Address = $(echo $NETADDRESS | cut -d . -f 1-3).$i/32
### DNS = 9.9.9.9
### 
### [Peer]
### # Server $NETNAME
### PublicKey = $(cat $WG_DIR/$NETNAME.pub)
### AllowedIPs = $NETADDRESS,0.0.0.0/0,$HOMENET
### Endpoint = $SERVERIP:$NETPORT
### PersistentKeepalive = 30
### EOF
###    # End create client config
### 
###    # update server config
###    cat <<EOF >>$WG_DIR/$NETNAME.conf
### [Peer]
### # Client cli${i}_$NETNAME
### PublicKey = $(cat $WG_DIR/cli${i}_$NETNAME.pub)
### AllowedIPs = $(echo $NETADDRESS | cut -d . -f 1-3).$i/32
### 
### EOF
###    # End update server config
### 
### done
### 

