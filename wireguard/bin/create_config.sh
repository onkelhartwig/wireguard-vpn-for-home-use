#!/bin/bash

# evaluate ip adresses of a network in CIDR notation
# expample: net_to_ip 192.168.10.0/24
#           192.168.10.0
#           192.168.10.1A
#           ...
#           192.168.10.255
net_to_ip() {
   BASE_IP=${1%/*}
   IP_CIDR=${1#*/}

   if [ ${IP_CIDR} -lt 8 ]; then
      echo "Max range is /8."
      exit
   fi
   
   # Notepad++ syntax highlighting interpretes the bitwise shift as input redirection
   # the operator "<<" will be masked therefor
   # The original statement is
   # IP_MASK=$((0xFFFFFFFF << (32 - ${IP_CIDR})))
   x="<<"
   IP_MASK=$((0xFFFFFFFF $x (32 - ${IP_CIDR})))

   IFS=. read a b c d <<<${BASE_IP}

   ip=$((($b << 16) + ($c << 8) + $d))

   ipstart=$((${ip} & ${IP_MASK}))
   ipend=$(((${ipstart} | ~${IP_MASK}) & 0x7FFFFFFF))

   seq ${ipstart} ${ipend} | while read i
   do
      echo $a.$((($i & 0xFF0000) >> 16)).$((($i & 0xFF00) >> 8)).$(($i & 0x00FF))
   done
}

create_client_keys() {
   # create key pairs for the clients
   # first create the TUNNEL_ONLY key pairs
   start=2
   startingip=$(net_to_ip $NETADDRESS | head -3 | tail -1)
   end=$(($start+$NUMCLIENTS_TUNNEL_ONLY+$NUMCLIENTS_COMPLETE_TRAFFIC-1))
   for ((i=$start;i<=$end;i++))
   do
      wg genkey >$WG_DIR/cli${i}_$NETNAME.priv
      wg pubkey <$WG_DIR/cli${i}_$NETNAME.priv >$WG_DIR/cli${i}_$NETNAME.pub
   done
}

create_server_config() {
   # Create server.conf
   # refer to etc/config.sh file and read the comments

   # The server always needs to be the first address in a network
   # examples
   #   network = 10.8.0.0/24 first IP is 10.8.0.1
   #   network = 10.7.0.0/24 first IP is 10.7.0.1
   #   network = 10.7.0.0/23 first IP is 10.6.0.1
   #   network = 10.8.0.128/25 first IP is 10.8.0.129
   #   network = 10.8.0.133/25 first IP is 10.8.0.129
   startingip=$(net_to_ip $NETADDRESS | head -2 | tail -1)
   network=$(net_to_ip $NETADDRESS | head -1 )
   servernetmask=$(echo $NETADDRESS | cut -d "/" -f2)
   serverip="$startingip/$servernetmask"

   cat <<EOF >>$WG_DIR/$NETNAME.conf
[Interface]
# Server $NETNAME
Address = $startingip/$servernetmask
SaveConfig = false
ListenPort = $NETPORT
PrivateKey = $(cat $WG_DIR/$NETNAME.priv)

EOF

}

create_client_config() {
   # refer to etc/config.sh file and read the comments
   startingip=$(net_to_ip $NETADDRESS | head -2 | tail -1)
   network=$(net_to_ip $NETADDRESS | head -1 )
   servernetmask=$(echo $NETADDRESS | cut -d "/" -f2)
   serverip="$startingip/$servernetmask"

   start=2
   end=$(($start+$NUMCLIENTS_TUNNEL_ONLY+$NUMCLIENTS_COMPLETE_TRAFFIC-1))
   for ((i=$start;i<=$end;i++))
   do
      if [ "$HOMENET" = "" ]
      then
         homemet=""
      else
         homenet=",$HOMENET"
      fi
      clientip=$(net_to_ip $network/$servernetmask | head -$(($i+1)) | tail -1)
      if [ "$i" -lt $(($start+$NUMCLIENTS_TUNNEL_ONLY)) ]
      then
         dns="# DNS = $DNS"
         allow="AllowedIPs = $network/$servernetmask$homenet"
      else
         dns="DNS = $DNS"
         allow="AllowedIPs = $network/$servernetmask$homenet",0.0.0.0/0
      fi
      cat <<EOF >$WG_DIR/cli${i}_$NETNAME.conf
[Interface]
# Client cli${i}_$NETNAME
PrivateKey = $(cat $WG_DIR/cli${i}_$NETNAME.priv)
Address = $clientip/$servernetmask
$dns

[Peer]
# Server $NETNAME
PublicKey = $(cat $WG_DIR/$NETNAME.pub)
$allow
Endpoint = $SERVERIP:$NETPORT
PersistentKeepalive = 30

EOF
      # Client config done, update server config
      cat <<EOF >>$WG_DIR/$NETNAME.conf
[Peer]
# Client cli${i}_$NETNAME
PublicKey = $(cat $WG_DIR/cli${i}_$NETNAME.pub)
AllowedIPs = $clientip/32

EOF
      #Server config updated
   done
}

create_csv_file() {
   # Create a .csv file to organize the client file distribution
   echo "" | awk -v OFS=$'\t' '{print "Name","Device","Client file","Tunnel scope"}' >$WG_DIR/$NETNAME.csv
   start=2
   startingip=$(net_to_ip $NETADDRESS | head -3 | tail -1)
   end=$(($start+$NUMCLIENTS_TUNNEL_ONLY+$NUMCLIENTS_COMPLETE_TRAFFIC-1))
   for ((i=$start;i<=$end;i++))
   do
      if [ $i -lt $(($NUMCLIENTS_TUNNEL_ONLY-1)) ]
      then
         tunnel_scope="split"
      else
         tunnel_scope="all_traffic"
      fi
      echo "cli${i}_$NETNAME.conf $tunnel_scope" | awk -v OFS=$'\t' '{print "","",$1,$2}' >>$WG_DIR/$NETNAME.csv
   done
}

create_qrcodes() {
   # Create a QR code (PNG file) for each client config file
   start=2
   startingip=$(net_to_ip $NETADDRESS | head -3 | tail -1)
   end=$(($start+$NUMCLIENTS_TUNNEL_ONLY+$NUMCLIENTS_COMPLETE_TRAFFIC-1))
   for ((i=$start;i<=$end;i++))
   do
      qrencode -o $WG_DIR/cli${i}_$NETNAME.png <$WG_DIR/cli${i}_$NETNAME.conf
   done
}


main:
# Set directories
BASEDIR="$(dirname $(dirname $0))"
BINDIR=$BASEDIR/bin
CONFDIR=$BASEDIR/etc
TMPDIR=$BASEDIR/tmp
VARDIR=$BASEDIR/var

# Read the config file
if [ "$1" = "" ]
then
   . $CONFDIR/config.sh
else
   . $CONFDIR/$1
   if [ "$?" != 0 ]
   then
      echo "error in configfile $CONFDIR/$1"
      exit 1
   fi
fi

# create the server config first
WG_DIR=$BASEDIR/$NETNAME
mkdir $WG_DIR
if [ "$?" -ne "0" ]
then
   exit 1
fi
echo Directory $WG_DIR created

# create key pairs for the server
umask 077
wg genkey >$WG_DIR/$NETNAME.priv
wg pubkey <$WG_DIR/$NETNAME.priv >$WG_DIR/$NETNAME.pub

# create key pairs for the clients
# first create the TUNNEL_ONLY key pairs
create_client_keys

# Create server.conf
create_server_config

# create client.conf files for TUNNEL_ONLY clients and update the server.conf
create_client_config

# Create a .csv file to organize the client file distribution
create_csv_file

# Create a QR code (PNG file) for each client config file
create_qrcodes

# Generate a compressed archive of the built directory
archname="$NETNAME.tgz"
cd "$WG_DIR"
tar -czf ../$archname ./
cd -
echo "Compressed archive $archname created in $PWD"



